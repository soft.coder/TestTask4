import { connect } from 'react-redux';
import App from './App';
import {isLogin} from './redux/actions';
import {withRouter} from 'react-router';


const mapDispatchToProps = (dispatch) => {
    return {
        checkUserIsLogin() {
            return dispatch(isLogin());
        }
    }
};
const mapStateToProps = state => {
    return {
        checkUserIsLoginInProgress: state.userState.inProgress
    }
};

const AppContainer = withRouter(connect(
    mapStateToProps,
    mapDispatchToProps
)(App));

export default AppContainer