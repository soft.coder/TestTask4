import React, { Component } from 'react';
import './App.css';
import {Route} from 'react-router-dom';
import LoginContainer from './app/Login/LoginContainer';
import RegistrationContainer from './app/Registration/RegistrationContainer';
import CreateTransactionContainer from './app/CreateTransaction/CreateTransactionContainer';
import NavBarContainer from './common/NavBar/NavBarContainer';
import TransactionsContainer from './app/Transactions/TransactionsContainer';


class App extends Component {
    constructor(props){
        super(props);
        this.props.checkUserIsLogin();
    }
    render() {
        let {checkUserIsLoginInProgress} = this.props;
        let result;
        if(!checkUserIsLoginInProgress) {
            result = (
                <div>
                    <NavBarContainer />
                    <Route exact path="/" component={TransactionsContainer} />
                    <Route path="/login" component={LoginContainer} />
                    <Route path="/registration" component={RegistrationContainer} />
                    <Route path="/create-transaction/" exact component={CreateTransactionContainer} />
                    <Route path="/create-transaction/:username/:amount" exact component={CreateTransactionContainer} />
                </div>
            );
        } else {
            result = (
                <div className="loading">
                    <p>Loading...</p>
                </div>
            );
        }
        return (
            <div className="App">
                {result}
            </div>
        );
    }
}

export default App;
