import {
    CHANGE_BALANCE,
    IS_LOGIN_FAILURE, IS_LOGIN_REQUEST, IS_LOGIN_SUCCESS,
    REMOVE_USER_INFO, REMOVE_USER_STATE,
    USER_INFO_FAILURE,
    USER_INFO_REQUEST,
    USER_INFO_SUCCESS
} from './actionTypes';
import axios from 'axios';
import {checkToken, removeToken} from '../utils';

export const isLogin = () => {
    return dispatch => {
        dispatch({type: IS_LOGIN_REQUEST});
        if(checkToken()) {
            return dispatch(userInfo()).then(() => {
                return dispatch({type: IS_LOGIN_SUCCESS,});
            }).catch(error => {
                return dispatch({type: IS_LOGIN_FAILURE, error});
            });
        } else {
            return dispatch({type: IS_LOGIN_FAILURE, error: 'User not login'});
        }
    }
};

export const logout = () => {
    return dispatch => {
        removeToken();
        dispatch({type: REMOVE_USER_STATE});
        dispatch({type: REMOVE_USER_INFO});
        return Promise.resolve();
    }
};

export const userInfo = () => {
    return dispatch => {
        dispatch({type: USER_INFO_REQUEST});
        return axios.get('/api/protected/user-info').then(({data: {user_info_token: userInfo}}) => {
            return dispatch({type: USER_INFO_SUCCESS, userInfo})
        }).catch(error => {
            if(error.response.status === 401) {
                removeToken();
            }
            return dispatch({type: USER_INFO_FAILURE, error})
        })
    }
};

export const changeBalance = (balance) => {
    return {type: CHANGE_BALANCE, balance};
};








