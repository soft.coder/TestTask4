import {combineReducers} from 'redux';
import {
    CHANGE_BALANCE,
    IS_LOGIN_FAILURE, IS_LOGIN_REQUEST, IS_LOGIN_SUCCESS,
    REMOVE_USER_INFO, REMOVE_USER_STATE,
    USER_INFO_FAILURE,
    USER_INFO_REQUEST,
    USER_INFO_SUCCESS,
} from './actionTypes';
import createTransaction from '../app/CreateTransaction/CreateTransactionReducer';
import transactions from '../app/Transactions/TransactionsReducer';
import registration from '../app/Registration/RegistrationReducer';
import login from '../app/Login/LoginReducer';


function userInfo(state = {}, action) {
    switch(action.type) {
        case USER_INFO_REQUEST:
            return Object.assign({}, state, {inProgress: true});
        case USER_INFO_SUCCESS:
            return Object.assign({}, state, {inProgress: false}, action.userInfo);
        case USER_INFO_FAILURE:
            return Object.assign({}, state, {inProgress: false});
        case CHANGE_BALANCE:
            return Object.assign({}, state, {balance: action.balance});
        case REMOVE_USER_INFO:
            return {};
        default:
            return state;
    }
}
function userState(state = {}, action) {
    switch(action.type){
        case IS_LOGIN_REQUEST:
            return Object.assign({}, state, {inProgress: true});
        case IS_LOGIN_SUCCESS:
            return Object.assign({}, state, {inProgress: false, isLogin: true});
        case IS_LOGIN_FAILURE:
            return Object.assign({}, state, {inProgress: false});
        case REMOVE_USER_STATE:
            return {};
        default:
            return state;
    }
}



let reducer = combineReducers({
    createTransaction,
    transactions,
    registration,
    login,
    userInfo,
    userState
});
export default reducer;