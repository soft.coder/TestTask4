import 'babel-polyfill';
import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import 'moment';
import 'react-datepicker/dist/react-datepicker.css';
import registerServiceWorker from './registerServiceWorker';
import 'bootstrap/dist/css/bootstrap.css';
import {BrowserRouter} from 'react-router-dom';
import thunkMiddleware from 'redux-thunk'
import {createStore, applyMiddleware} from 'redux';
import reducer from './redux/reducers';
import {Provider} from 'react-redux';
import AppContainer from './AppContainer';
import * as axios from 'axios';
import {baseUrl} from './constants';

axios.defaults.baseURL = baseUrl;

let store = createStore(reducer, applyMiddleware(thunkMiddleware));

ReactDOM.render(<Provider store={store}><BrowserRouter><AppContainer /></BrowserRouter></Provider>, document.getElementById('root'));
registerServiceWorker();
