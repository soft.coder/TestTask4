import axios from 'axios';

export const saveToken = token => {
    window.localStorage.setItem('id_token', token);
    axios.defaults.headers.common['Authorization'] = `Bearer ${token}`;
};
export const checkToken = () => {
    let token = window.localStorage.getItem('id_token');
    if(token) {
        axios.defaults.headers.common['Authorization'] = `Bearer ${token}`;
    }
    return !!token;
};
export const removeToken = () => {
    delete axios.defaults.headers.common['Authorization'];
    window.localStorage.removeItem('id_token')
};