import React, {Component} from 'react';
import FilterHeaderText from '../FilterHeaderText/FilterHeaderText';


class FilterHeaderNumRange extends Component {
    constructor(props) {
        super(props);
        this.onChange = this.onChange.bind(this);
    }

    onChange(filter) {
        let [start, end] = filter.split('<>');
        if(this.props.onChange) {
            this.props.onChange(parseFloat(start), parseFloat(end));
        }
    }


    render() {
        return (
            <FilterHeaderText className="text-input" focus={this.props.focus} name={this.props.name}
                              placeholder="10<>45"
                              onChange={this.onChange}/>
        );
    }
}

export default FilterHeaderNumRange;
