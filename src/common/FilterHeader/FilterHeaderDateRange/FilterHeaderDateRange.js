import React, {Component} from 'react';
import './FilterHeaderDateRange.css';
import DatePicker from 'react-datepicker';
import cn from 'classnames';

class FilterHeaderDateRange extends Component {
    constructor(props) {
        super(props);
        this.state = {};
        this.toggleCalendarStart = this.toggleCalendarStart.bind(this);
        this.toggleCalendarEnd = this.toggleCalendarEnd.bind(this);
        this.handleChangeStart = this.handleChangeStart.bind(this);
        this.handleChangeEnd = this.handleChangeEnd.bind(this);
    }
    toggleCalendarStart() {
        this.setState(prevState => ({isOpenStart: !prevState.isOpenStart}));
    }
    toggleCalendarEnd() {
        this.setState(prevState => ({isOpenEnd: !prevState.isOpenEnd}));
    }
    handleChangeStart(startDate) {
        this.setState({startDate, isOpenStart: false});
        this.props.onChange(startDate, this.state.endDate);

    }
    handleChangeEnd(endDate) {
        this.setState({endDate, isOpenEnd: false});
        this.props.onChange(this.state.startDate, endDate);
    }
    static getDerivedStateFromProps(props) {
        if(props.open === false) {
            return {
                isOpenStart: undefined,
                isOpenEnd: undefined,
                startDate: undefined,
                endDate: undefined
            }
        } else {
            return {};
        }
    }
    render() {
        return (
            <div className={cn("filter-header-date-range", this.props.className)}>
                <div>
                    <DatePicker
                        selected={this.state.startDate}
                        customInput={<button><span title="Enter start date" className="fa fa-calendar"></span></button>}
                        selectsStart showMonthDropdown showYearDropdown dropdownMode="select"
                        maxDate={this.state.endDate}
                        startDate={this.state.startDate}
                        endDate={this.state.endDate}
                        onChange={this.handleChangeStart}/>
                    <DatePicker
                        customInput={<button><span title="Enter end date" className="fa fa-calendar"></span></button>}
                        selected={this.state.endDate}
                        selectsEnd showMonthDropdown showYearDropdown dropdownMode="select"
                        minDate={this.state.startDate}
                        startDate={this.state.startDate}
                        endDate={this.state.endDate}
                        onChange={this.handleChangeEnd}/>
                </div>
            </div>
        );
    }
}

export default FilterHeaderDateRange;
