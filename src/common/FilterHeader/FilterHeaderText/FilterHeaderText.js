import React, {Component} from 'react';
import './FilterHeaderText.css';
import cn from 'classnames';

class FilterHeaderText extends Component {
    constructor(props) {
        super(props);
        this.onChange = this.onChange.bind(this);
        this.updateFilter = this.updateFilter.bind(this);
        this.inputRef =  React.createRef();
    }
    updateFilter(filter) {
        this.props.onChange(filter);
    }
    onChange() {
        let deBounce = this.props.debounce !== undefined ? this.props.debounce : 200;
        if(this.props.onChange && this.props.debounce !== 0) {
            if(this.debounceTimer) {
                clearTimeout(this.debounceTimer);
            }
            this.debounceTimer = setTimeout(() => this.updateFilter(this.inputRef.value), deBounce);
        }
    }
    componentWillUnmount() {
        clearTimeout(this.debounceTimer);
    }
    componentDidUpdate() {
        if(this.props.focus !== undefined) {
            if(this.props.focus && !this.isAlreadyFocus) {
                this.inputRef.focus();
                this.isAlreadyFocus = true;
            } else if(!this.props.focus && this.isAlreadyFocus){
                this.inputRef.blur();
                this.inputRef.value = '';
                this.isAlreadyFocus = false;
            }
        }
    }
    render() {
        return (
            <input type="text" className={cn("filter-header-text form-control form-control-sm form-control-plaintext", this.props.className)}
                   ref={input => this.inputRef = input}
                   placeholder={this.props.placeholder ? this.props.placeholder : 'Enter ' + this.props.name.toLowerCase()}
                   onChange={this.onChange}/>
        );
    }
}

export default FilterHeaderText;
