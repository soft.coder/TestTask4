import React, {Component} from 'react';
import './FilterHeader.css';
import cn from 'classnames';
import FilterHeaderText from './FilterHeaderText/FilterHeaderText';
import FilterHeaderDateRange from './FilterHeaderDateRange/FilterHeaderDateRange';
import FilterHeaderNumRange from './FilterHeaderNumRange/FilterHeaderNumRange';


class FilterHeader extends Component {
    constructor(props) {
        super(props);
        this.state = {isOpen: false, filterText: ''};
        this.onChangeOrder = this.onChangeOrder.bind(this);
        this.onChangeFilter = this.onChangeFilter.bind(this);
        this.onOpenChange = this.onOpenChange.bind(this);
        this.inputRef =  React.createRef();
    }
    onChangeOrder() {
        if(this.props.onChangeOrder) {
            this.props.onChangeOrder(this.props.name.toLowerCase());
        }
    }
    onChangeFilter(...filter) {
        if(this.props.onChangeFilter) {
            this.props.onChangeFilter(this.props.name.toLowerCase(), ...filter);
        }
    }
    onOpenChange() {
        this.setState(prevState => {
            if(prevState.isOpen) {
                this.props.onClose(this.props.name.toLowerCase());
                return {isOpen: false, focus: false};
            } else {
                return {isOpen: true, focus: true}
            }
        });
    }

    render() {
        let sortCn = cn('fa filter-header__sort-icon', {
            'fa-sort-amount-desc': this.props.order || this.props.order === undefined,
            'fa-sort-amount-asc': !this.props.order,
            'text-success': this.props.order !== undefined
        });
        let filterHeaderCn = cn("filter-header flex align-items-center", this.props.type, {
            'is-open': this.state.isOpen,
        });
        let filterHeader;
        switch(this.props.type) {
            case 'date-range':
                filterHeader = <FilterHeaderDateRange className="date-range" name={this.props.name} open={this.isOpen}
                                                      onChange={this.onChangeFilter}/>;
                break;
            case 'num-range':
                filterHeader = <FilterHeaderNumRange className="text-input" focus={this.state.focus} name={this.props.name}
                                                     onChange={this.onChangeFilter}/>;
                 break;
            case 'text':
                filterHeader = <FilterHeaderText className="text-input" focus={this.state.focus} name={this.props.name}
                                                 onChange={this.onChangeFilter}/>;
                break;
            default:
                break;
        }
        return (
            <div className={filterHeaderCn}>
                <p>{this.props.name}</p>
                {filterHeader}
                <button className={sortCn} onClick={this.onChangeOrder} ></button>
                <button className='fa fa-navicon' onClick={this.onOpenChange}></button>
            </div>
        );
    }
}

export default FilterHeader;
