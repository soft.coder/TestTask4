import React from 'react';

const HighlightText = ({text, value}) => {
    let resultText = [];
    let strText = text !== undefined ? text.toString() : text;
    let strValue = value !== undefined ? value.toString() : value;
    if(!strValue) {
        resultText.push(strText);
    } else {
        let index, prevIndex = 0;

        while((index = strText.indexOf(strValue, prevIndex)) !== -1) {
            resultText.push(strText.substr(prevIndex, index));
            resultText.push(<span key={index} className="font-weight-bold">{strValue}</span>);
            prevIndex = index + strValue.length;
        }
        resultText.push(strText.substr(prevIndex));
    }
    return (
        <div className="HighlightText">
            {resultText}
        </div>
    );
};

export default HighlightText;
