import React, {Component} from 'react';
import './NavBar.css';
import {NavLink} from 'react-router-dom';
import cn from 'classnames';
import {Button, Nav, NavItem, Navbar as RsNavbar, Collapse, Tooltip} from 'reactstrap';




class NavBar extends Component{
    constructor(props) {
        super(props);
        this.logout = this.logout.bind(this);
    }
    logout() {
        return this.props.logout();
    }
    render() {
        let {registrationInProgress, loginInProgress, name, balance, isLogin} = this.props;
        return (
            <div>
                <RsNavbar color="dark" dark className="navbar-expand-lg">
                    <Collapse isOpen={true} navbar>
                        <Nav className="navbar-nav">
                            <NavItem>
                                <NavLink activeClassName="active" className={cn('nav-link', {'d-none': !isLogin})} to="/">
                                    Transactions
                                </NavLink>
                            </NavItem>
                            <NavItem>
                                <NavLink activeClassName="active"
                                         className={cn('nav-link', {'d-none': !isLogin})}
                                         to="/create-transaction">
                                    Create transaction
                                </NavLink>
                            </NavItem>
                            <NavItem>
                                <NavLink activeClassName="active" id="nav_link_login"
                                         onClick={e => registrationInProgress ? e.preventDefault() : null}
                                         className={cn('nav-link', {'d-none': isLogin})} to="/login">
                                    Login
                                </NavLink>
                                <Tooltip placement="right" target="nav_link_login" >
                                    Hello world!
                                </Tooltip>
                            </NavItem>
                            <NavItem>
                                <NavLink activeClassName="active" disabled={true}
                                         onClick={e => loginInProgress ? e.preventDefault() : null}
                                         className={cn('nav-link', {'d-none': isLogin})}
                                         to="/registration">
                                    Registration
                                </NavLink>
                            </NavItem>
                        </Nav>
                        <div className="text-right flex-grow-1 d-flex align-items-center justify-content-end">
                            <div className="navbar-text mr-2">
                                <span className={cn({'d-none': !isLogin})}>
                                    {`${name} (${balance}$)`}
                                </span>
                            </div>
                            <Button color="secondary" size="sm" className={cn({'d-none': !isLogin})} onClick={this.logout} >Logout</Button>
                        </div>
                    </Collapse>>
                </RsNavbar>
            </div>
        );
    }
}

export default NavBar;
