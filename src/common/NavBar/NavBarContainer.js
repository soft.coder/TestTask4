import { connect } from 'react-redux';
import NavBar from './NavBar';
import {logout} from '../../redux/actions';
import {withRouter} from 'react-router';


const mapDispatchToProps = (dispatch) => {
    return {
        logout() {
            return dispatch(logout());
        }
    }
};
const mapStateToProps = state => {
    return {
        name: state.userInfo.name,
        balance: state.userInfo.balance,
        registrationInProgress: state.registration.inProgress,
        loginInProgress: state.login.inProgress,
        isLogin: state.userState.isLogin
    }
};

const NavBarContainer = withRouter(connect(
    mapStateToProps,
    mapDispatchToProps
)(NavBar));

export default NavBarContainer