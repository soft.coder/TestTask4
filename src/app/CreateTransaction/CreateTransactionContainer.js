import { connect } from 'react-redux';
import CreateTransaction from './CreateTransaction';
import {createTransaction, nameSuggestions, verifyName} from './CreateTransacitonActions';


const mapDispatchToProps = (dispatch) => {
    return {
        createTransaction(name, amount) {
            return dispatch(createTransaction(name, amount));
        },
        getNameSuggestions(filter) {
            dispatch(nameSuggestions(filter));
        },
        verifyName(name) {
            return dispatch(verifyName(name));
        }
    }
};
const mapStateToProps = state => {
    return {
        transactionInProgress: state.createTransaction.transaction.inProgress,
        serverErrorMsg: state.createTransaction.transaction.error && state.createTransaction.transaction.error.response.data,
        suggestions: state.createTransaction.suggestions.list,
        isLoadingSuggestions: state.createTransaction.suggestions.inProgress,
        isValidName: state.createTransaction.verifyName.isValidName,
        isLogin: state.userState.isLogin,
        isLoginInProgress: state.userState.inProgress,
        balance: state.userInfo.balance
    }
};

const CreateTransactionContainer = connect(
    mapStateToProps,
    mapDispatchToProps
)(CreateTransaction);

export default CreateTransactionContainer