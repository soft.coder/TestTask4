import {
    CREATE_TRANSACTION_FAILURE, CREATE_TRANSACTION_REQUEST,
    CREATE_TRANSACTION_SUCCESS, NAME_SUGGESTIONS_FAILURE, NAME_SUGGESTIONS_REQUEST, NAME_SUGGESTIONS_SUCCESS,
    VERIFY_NAME_FAILURE,
    VERIFY_NAME_REQUEST, VERIFY_NAME_SUCCESS
} from './CreateTransactionActionTypes';
import {combineReducers} from 'redux';

function transaction(state = {}, action) {
    switch(action.type) {
        case CREATE_TRANSACTION_REQUEST:
            return Object.assign({}, state, {
                inProgress: true,
                name: action.name,
                amount: action.amount,
                error: undefined
            });
        case CREATE_TRANSACTION_SUCCESS:
            return Object.assign({}, state, {
                inProgress: false,
                name: undefined,
                amount: undefined,
                error: undefined,
                transaction: action.transaction
            });
        case CREATE_TRANSACTION_FAILURE:
            return Object.assign({}, state, {
                inProgress: false,
                error: action.error
            });
        default:
            return state;
    }
}
function suggestions(state = {list: [], inProgress: false}, action) {
    switch(action.type) {
        case NAME_SUGGESTIONS_REQUEST:
            return Object.assign({}, state, {
                inProgress: true
            });
        case NAME_SUGGESTIONS_SUCCESS:
            return Object.assign({}, state, {
                inProgress: false,
                list: action.suggestions,
                error: undefined
            });
        case NAME_SUGGESTIONS_FAILURE:
            return Object.assign({}, state, {
                inProgress: false,
                error: action.error
            });
        default:
            return state;
    }
}
function verifyName(state = {}, action){
    switch(action.type){
        case VERIFY_NAME_REQUEST:
            return state;
        case VERIFY_NAME_SUCCESS:
            return Object.assign({}, state, {
                isValidName: action.isValid,
                error: undefined
            });
        case VERIFY_NAME_FAILURE:
            return Object.assign({}, state, {error: action.error});
        default:
            return state;
    }
}

let createTransaction = combineReducers({
    transaction,
    suggestions,
    verifyName
});
export default createTransaction;