import {
    CREATE_TRANSACTION_FAILURE,
    CREATE_TRANSACTION_REQUEST, CREATE_TRANSACTION_SUCCESS,
    NAME_SUGGESTIONS_FAILURE, NAME_SUGGESTIONS_REQUEST, NAME_SUGGESTIONS_SUCCESS, VERIFY_NAME_FAILURE,
    VERIFY_NAME_REQUEST, VERIFY_NAME_SUCCESS
} from './CreateTransactionActionTypes';
import axios from 'axios';
import {changeBalance} from '../../redux/actions';

export const nameSuggestions = (filter) => {
    return dispatch => {
        dispatch({type: NAME_SUGGESTIONS_REQUEST});
        return axios.post('/api/protected/users/list', {filter}).then((response) => {
            dispatch({type: NAME_SUGGESTIONS_SUCCESS, suggestions: response.data});
        }).catch(error => {
            dispatch({type: NAME_SUGGESTIONS_FAILURE, error})
        })
    }
};
export const verifyName = (name) => {
    return dispatch => {
        dispatch({type: VERIFY_NAME_REQUEST});
        return axios.post('/api/protected/users/list', {filter: name}).then((response) => {
            let isValid = response.data.some(u => u.name === name);
            dispatch({type: VERIFY_NAME_SUCCESS, isValid});
            return Promise.resolve();
        }).catch(error => {
            dispatch({type: VERIFY_NAME_FAILURE, error});
            return Promise.reject();
        })
    }
};
export const createTransaction = (name, amount) => {
    return dispatch => {
        dispatch({type: CREATE_TRANSACTION_REQUEST, name, amount});
        return axios.post('/api/protected/transactions', {name, amount}).then(({data}) => {
            dispatch({type: CREATE_TRANSACTION_SUCCESS});
            dispatch(changeBalance(data.trans_token.balance));
            return Promise.resolve(data);
        }).catch(error => {
            dispatch({type: CREATE_TRANSACTION_FAILURE, error});
            return Promise.reject(error);
        });
    }
};