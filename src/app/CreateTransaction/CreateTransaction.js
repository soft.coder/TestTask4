import React, {Component} from 'react';
import './CreateTransaction.css';
import {Alert, Button, Form, FormFeedback, FormGroup, Input, Label} from 'reactstrap';
import {AsyncTypeahead} from 'react-bootstrap-typeahead';
import cn from 'classnames';
import {Redirect} from 'react-router';



class CreateTransaction extends Component {
    constructor(props) {
        super(props);
        let {username = '', amount = ''} = this.props.match.params;
        if(username && amount) {
            this.state = {
                userName: username, verifyNameInProgress: true,
                amount: Number.isNaN(parseFloat(amount)) ? '' : parseFloat(amount),
            };
        } else {
            this.state = {userName: username, amount: amount};
        }
        this.onChangeUserName = this.onChangeUserName.bind(this);
        this.onChangeAmount = this.onChangeAmount.bind(this);
        this.onSubmit = this.onSubmit.bind(this);
        this.onSearch = this.onSearch.bind(this);
        this.onBlurUserName = this.onBlurUserName.bind(this);
    }
    onChangeUserName(options) {
        this.setState({userName: options[0] && options[0].name, userNameDirty: true});
    }
    onBlurUserName() {
        if(!this.props.verifyComplete) {
            this.setState({userNameDirty: true});
        }
    }
    onChangeAmount(event) {
        if(!event.target.validity.patternMismatch) {
            this.setState({amount: event.target.value, amountDirty: true});
        }
    }
    onSubmit(event) {
        this.setState({isSubmit: true});
        event.preventDefault();
        this.props.createTransaction(this.state.userName, this.state.amount).then(() => {
            !this.isUnmount && this.setState({transactionComplete: true});
            this.timerSuccess = setTimeout(() => {
                !this.isUnmount && this.setState({transactionComplete: false});
            }, 1000);
        }).catch(() => {});
    }
    onSearch(filter) {
        this.props.getNameSuggestions(filter)
    }
    componentDidMount() {
        if(this.state.verifyNameInProgress) {
            this.props.verifyName(this.state.userName).then(() => {
                !this.isUnmount && this.setState({verifyComplete: true, verifyNameInProgress: false});
            });
        }
    }
    componentWillUnmount() {
        clearInterval(this.timerSuccess);
        this.isUnmount = true;
    }
    render() {
        let {suggestions, isLoadingSuggestions, isLogin, isValidName,
            serverErrorMsg, isLoginInProgress,
            transactionInProgress} = this.props;
        let {verifyComplete, verifyNameInProgress} = this.state;
        if(isLoginInProgress === false && !isLogin || isLoginInProgress === undefined && isLogin === undefined) {
            return <Redirect to="/login" />
        }
        let selected = [];
        if(this.state.userName) {
            selected.push(this.state.userName);
        }

        let errorMsgUsername = !this.state.userName ? 'Please provide username' : 'Invalid username';
        let errorMsgAmount = !this.state.amount ? 'Please provide amount' : 'Amount exceed your balance';
        let isInvalidName = !this.state.userName;
        let isInvalidAmount = !this.state.amount || parseFloat(this.state.amount) > this.props.balance;
        let isInvalidVerify = verifyNameInProgress !== undefined && verifyComplete && !isValidName;
        let isInvalidRbt = (this.state.isSubmit || this.state.userNameDirty) && isInvalidName ||
                            (!this.state.userNameDirty && isInvalidVerify);
        let isValidRbt = (this.state.isSubmit || this.state.userNameDirty) && !isInvalidName ||
                        (!verifyNameInProgress && !this.state.userNameDirty && verifyComplete && isValidName);
        let rbtClass = cn({
            'is-invalid': isInvalidRbt,
            'is-valid': isValidRbt,
            'is-verifying': verifyNameInProgress
        });

        return (
            <div className="create-transaction">
                <h4 className="text-center">
                    Create transaction
                </h4>
                <Alert color="danger" isOpen={!!serverErrorMsg}>
                    {serverErrorMsg}
                </Alert>
                <Alert color="success" isOpen={!!this.state.transactionComplete}>
                    Transaction successfully executed
                </Alert>
                <Form>
                    <FormGroup>
                        <Label for="username">Username</Label>
                        <AsyncTypeahead
                            autofocus={true}
                            id="username"
                            isLoading={isLoadingSuggestions}
                            disabled={verifyNameInProgress || transactionInProgress}
                            labelKey="name"
                            options={suggestions}
                            selected={selected}
                            className={rbtClass}
                            onChange={this.onChangeUserName}
                            onBlur={this.onBlurUserName}
                            onSearch={this.onSearch}
                            placeholder="Search for a user..."
                        />
                        <FormFeedback>{errorMsgUsername}</FormFeedback>
                        <FormFeedback valid>Good!</FormFeedback>
                        <FormFeedback valid className='text-warning'>Verifying name...</FormFeedback>
                    </FormGroup>
                    <FormGroup>
                        <Label for="amount">Amount</Label>
                        <Input id="amount" type="text" required pattern="\d*\.?\d*"
                               autoComplete="off" disabled={transactionInProgress}
                               invalid={(this.state.isSubmit || this.state.amountDirty) && isInvalidAmount}
                               valid={!isInvalidAmount}
                               value={this.state.amount} onChange={this.onChangeAmount}/>
                        <FormFeedback>{errorMsgAmount}</FormFeedback>
                        <FormFeedback valid>Good!</FormFeedback>
                    </FormGroup>
                    <FormGroup className="text-right">
                        <Button color="primary" type="submit"
                                disabled={transactionInProgress || isInvalidName || isInvalidAmount || verifyNameInProgress || isInvalidVerify}
                                onClick={this.onSubmit}>Send</Button>
                    </FormGroup>
                </Form>
            </div>
        );
    }
}

export default CreateTransaction;
