import axios from 'axios';
import {LOGIN_FAILURE, LOGIN_REQUEST, LOGIN_SUCCESS} from './LoginActionTypes';
import {saveToken} from '../../utils';


export const login = (email, password) => {
    return dispatch => {
        dispatch({type: LOGIN_REQUEST, email});
        return axios.post('/sessions/create', {email, password}).then(({data: {id_token}}) => {
            saveToken(id_token);
            return dispatch({type: LOGIN_SUCCESS});
        }).catch(error => {
            return dispatch({type: LOGIN_FAILURE, error});
        });
    }
};