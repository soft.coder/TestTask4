import { connect } from 'react-redux';
import Login from './Login';
import {isLogin} from '../../redux/actions';
import {withRouter} from 'react-router';
import {login} from './LoginActions';


const mapDispatchToProps = (dispatch) => {
    return {
        login(email, password) {
            dispatch(login(email, password)).then(() => {
                return dispatch(isLogin());
            });
        }
    }
};
const mapStateToProps = state => {
    return {
        inProgress: state.login.inProgress,
        email: state.login.email,
        isLogin: state.userState.isLogin,
        serverErrorMsg: state.login.error && state.login.error.response.data
    }
};

const LoginContainer = withRouter(connect(
    mapStateToProps,
    mapDispatchToProps
)(Login));

export default LoginContainer