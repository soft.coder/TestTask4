import {LOGIN_FAILURE, LOGIN_REQUEST, LOGIN_SUCCESS} from './LoginActionTypes';

function login(state = {}, action) {
    switch(action.type){
        case LOGIN_REQUEST:
            return Object.assign({}, state, {
                inProgress: true,
                email: action.email
            });
        case LOGIN_SUCCESS:
            return Object.assign({}, state, {inProgress: false, email: undefined, error: undefined});
        case LOGIN_FAILURE:
            return Object.assign({}, state, {inProgress: false, error: action.error});
        default:
            return state;
    }
}

export default login;