import React, {Component} from 'react';
import './Login.css';
import {Alert, Button, Form, FormFeedback, FormGroup, Input, Label} from 'reactstrap';
import {Redirect} from 'react-router';

class Login extends Component{
    constructor(props) {
        super(props);
        this.state = {email: this.props.email || '', password: ''};
        this.onChangeEmail = this.onChangeEmail.bind(this);
        this.onChangePassword = this.onChangePassword.bind(this);
        this.onLogin = this.onLogin.bind(this);
        this.onBlurEmail = this.onBlurEmail.bind(this);
        this.onBlurPassword = this.onBlurPassword.bind(this);
    }
    onChangeEmail(event) {
        this.setState({email: event.target.value, emailInvalid: event.target.validity.typeMismatch});
    }
    onBlurEmail() {
        this.setState({emailDirty: true});
    }
    onChangePassword(event) {
        this.setState({password: event.target.value, passwordDirty: true});
    }
    onBlurPassword(event) {
        this.setState({passwordDirty: true});
    }
    onLogin(event) {
        this.setState({isSubmit: true});
        this.props.login(this.state.email, this.state.password);
        event.preventDefault();
    }
    onClose(event) {
        this.setState({isSubmit: true});
    }
    render() {
        let {inProgress, isLogin, serverErrorMsg} = this.props;
        let {isSubmit, emailDirty, passwordDirty} = this.state;
        if(isLogin) {
            return <Redirect to='/' />
        }
        let invalidEmail = !this.state.email || this.state.emailInvalid;
        let invalidPassword = !this.state.password;
        let emailErrorMsg = !this.state.email ? 'Please provide email' : 'Please provide valid email';
        return (
            <div className="login">
                <h3 className="text-center">Login</h3>
                <Alert color="danger" isOpen={!!serverErrorMsg}>
                    {serverErrorMsg}
                </Alert>
                <Form onSubmit={this.onRegistration}>
                    <FormGroup>
                        <Label for="username">Email</Label>
                        <Input id="username" type="email" required
                               disabled={inProgress}
                               invalid={(isSubmit || emailDirty) && invalidEmail}
                               placeholder="email" value={this.state.email}
                               onBlur={this.onBlurEmail}
                               onChange={this.onChangeEmail} />
                        <FormFeedback>{emailErrorMsg}</FormFeedback>
                    </FormGroup>
                    <FormGroup>
                        <Label for="password">Password</Label>
                        <Input id="password" type="password" required
                               disabled={inProgress} onBlur={this.onBlurPassword}
                               invalid={(isSubmit || passwordDirty) && invalidPassword} placeholder="password"
                               value={this.state.password} onChange={this.onChangePassword} />
                        <FormFeedback>Please provide password</FormFeedback>
                    </FormGroup>
                    <FormGroup className="text-right">
                        <Button type="submit" color="primary" disabled={inProgress || invalidEmail || invalidPassword} onClick={this.onLogin}>Login</Button>
                    </FormGroup>
                </Form>
            </div>
        );
    }
}

export default Login;
