import { connect } from 'react-redux';
import Registration from './Registration';
import {withRouter} from 'react-router';
import {registration} from './RegistrationActions';

const mapDispatchToProps = (dispatch) => {
    return {
        registration(name, email, password) {
            dispatch(registration(name, email, password));
        }
    }
};
const mapStateToProps = state => {
    return {
        inProgress: state.registration.inProgress,
        name: state.registration.name,
        serverErrorMsg: state.registration.error && state.registration.error.response.data,
        email: state.registration.email,
        isLogin: state.userState.isLogin
    }
};

const RegistrationContainer = withRouter(connect(
    mapStateToProps,
    mapDispatchToProps
)(Registration));

export default RegistrationContainer