import {REGISTRATION_FAILURE, REGISTRATION_REQUEST, REGISTRATION_SUCCESS} from './RegistrationActionTypes';

function registration(state = {}, action){
    switch(action.type){
        case REGISTRATION_REQUEST:
            return Object.assign({}, state, {
                inProgress: true,
                name: action.name,
                email: action.email
            });
        case REGISTRATION_SUCCESS:
            return Object.assign({}, state, {
                inProgress: false,
                name: undefined,
                email: undefined,
                error: undefined
            });
        case REGISTRATION_FAILURE:
            return Object.assign({}, state, {
                error: action.error,
                inProgress: false,
            });
        default:
            return state;
    }
}
export default registration;