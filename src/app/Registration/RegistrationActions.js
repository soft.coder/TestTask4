import {REGISTRATION_FAILURE, REGISTRATION_REQUEST, REGISTRATION_SUCCESS} from './RegistrationActionTypes';
import {isLogin} from '../../redux/actions';
import {login} from '../Login/LoginActions';
import axios from 'axios';
import {saveToken} from '../../utils';

export const registration = (name, email, password) => {
    return dispatch => {
        dispatch({type: REGISTRATION_REQUEST});
        return axios.post('/users', {username: name, email, password}).then(({id_token}) => {
            saveToken(id_token);
            return dispatch({type: REGISTRATION_SUCCESS, name});
        }).then(() => {
            return dispatch(login(email, password));
        }).then(() => {
            return dispatch(isLogin());
        }).catch(error => {
            dispatch({type: REGISTRATION_FAILURE, error});
            return Promise.reject(error);
        });
    }
};
