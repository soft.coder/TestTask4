import React, {Component} from 'react';
import './Registration.css';
import {Form, FormFeedback, Button, Input, FormGroup, Label, Alert} from 'reactstrap';
import {Redirect} from 'react-router';

class Registration extends Component{
    constructor(props) {
        super(props);
        let {name = '', email = ''} = this.props;
        this.state = {name, email, password: '', passwordRepeat: ''};
        this.onChangeUserName = this.onChangeUserName.bind(this);
        this.onChangeEmail = this.onChangeEmail.bind(this);
        this.onBlurEmail = this.onBlurEmail.bind(this);
        this.onChangePassword = this.onChangePassword.bind(this);
        this.onChangePasswordRepeat = this.onChangePasswordRepeat.bind(this);
        this.onRegistration = this.onRegistration.bind(this);
    }
    onChangeUserName(event) {
        this.setState({name: event.target.value, nameDirty: true});
    }
    onChangeEmail(event) {
        this.setState({email: event.target.value, emailInvalid: event.target.validity.typeMismatch});
    }
    onBlurEmail(event) {
        this.setState({emailDirty: true});
    }
    onChangePassword(event) {
        this.setState({password: event.target.value, passwordDirty: true});
    }
    onChangePasswordRepeat(event) {
        this.setState({passwordRepeat: event.target.value, passwordRepeatDirty: true});
    }
    onRegistration(event) {
        this.setState({isSubmit: true});
        let {name, email, password} = this.state;
        this.props.registration(name, email, password);
        event.preventDefault();
    }
    render() {
        let {inProgress, isLogin, serverErrorMsg} = this.props;
        let {isSubmit, nameDirty, emailDirty, passwordDirty, passwordRepeatDirty} = this.state;
        if(isLogin) {
            return <Redirect to='/' />
        }
        let repeatErrorMsg = !this.state.passwordRepeat ? 'Required' : 'Passwords do not match';
        let emailErrorMsg = !this.state.email ? 'Please provide email' : 'Please provide valid email';
        let isInvalidName = !this.state.name;
        let isInvalidEmail = (!this.state.email || this.state.emailInvalid);
        let isInvalidPassword = !this.state.password;
        let isInvalidPasswordRepeat = !this.state.passwordRepeat || (
                this.state.passwordDirty &&
                this.state.passwordRepeat.length >= this.state.password.length &&
                this.state.password !== this.state.passwordRepeat
        );
        let isFormInvalid = isInvalidName || isInvalidEmail || isInvalidPassword || isInvalidPasswordRepeat;
        return (
            <div className="registration">
                <h3 className="text-center">Registration</h3>
                <Alert color="danger" isOpen={!!serverErrorMsg}>
                    {serverErrorMsg}
                </Alert>
                <Form onSubmit={this.onRegistration}>
                    <FormGroup>
                        <Label for="name">Name<span className="text-danger">*</span></Label>
                        <Input id="name" type="text" required
                               disabled={inProgress}
                               invalid={(isSubmit || nameDirty) && isInvalidName}
                               placeholder="name" value={this.state.name} onChange={this.onChangeUserName} />
                        <FormFeedback>Please provide name</FormFeedback>
                    </FormGroup>
                    <FormGroup>
                        <Label for="email">Email<span className="text-danger">*</span></Label>
                        <Input id="email" type="email" required placeholder="email"
                               disabled={inProgress}
                               invalid={(isSubmit || emailDirty) && isInvalidEmail}
                               value={this.state.email} onChange={this.onChangeEmail} onBlur={this.onBlurEmail} />
                        <FormFeedback>{emailErrorMsg}</FormFeedback>
                    </FormGroup>
                    <FormGroup>
                        <Label for="password">Password<span className="text-danger">*</span></Label>
                        <Input id="password" type="password" required placeholder="password"
                               disabled={inProgress}
                               invalid={(isSubmit || passwordDirty) && isInvalidPassword}
                               value={this.state.password} onChange={this.onChangePassword} />
                        <FormFeedback>Please provide password</FormFeedback>
                    </FormGroup>
                    <FormGroup>
                        <Label for="password_repeat">Repeat password<span className="text-danger">*</span></Label>
                        <Input id="password_repeat" type="password" required
                               disabled={inProgress}
                               invalid={(isSubmit || passwordRepeatDirty) && isInvalidPasswordRepeat} placeholder="name"
                               value={this.state.passwordRepeat} onChange={this.onChangePasswordRepeat} />
                        <FormFeedback>{repeatErrorMsg}</FormFeedback>
                    </FormGroup>
                    <FormGroup className="text-right">
                        <Button type="submit" color="primary"
                                disabled={inProgress || isFormInvalid}
                                onClick={this.onRegistration}>Registration</Button>
                    </FormGroup>
                </Form>
            </div>
        );
    }
}

export default Registration;
