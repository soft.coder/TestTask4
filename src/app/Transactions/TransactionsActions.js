import axios from 'axios';
import {TRANSACTIONS_FAILURE, TRANSACTIONS_REQUEST, TRANSACTIONS_SUCCESS} from './TransactionsActionTypes';

export const transactions = () => {
    return dispatch => {
        dispatch({type: TRANSACTIONS_REQUEST});
        return axios.get('/api/protected/transactions').then((response) => {
            dispatch({type: TRANSACTIONS_SUCCESS, list: response.data.trans_token});
        }).catch(error => {
            dispatch({type: TRANSACTIONS_FAILURE, error});
        });
    }
};