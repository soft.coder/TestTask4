import React, {Component} from 'react';
import './Transactions.css';
import {Table} from 'reactstrap';
import {Link} from 'react-router-dom';
import {Redirect} from 'react-router';
import FilterHeader from '../../common/FilterHeader/FilterHeader';
import moment from 'moment';
import HighlightText from '../../common/HighlightText/HighlightText';

class Transactions extends Component{
    constructor(props) {
        super(props);
        this.props.transactions();
        this.state = {order: {}, filter: {}};
        this.onChangeOrder = this.onChangeOrder.bind(this);
        this.onChangeFilter = this.onChangeFilter.bind(this);
        this.onChangeFilterDateRange = this.onChangeFilterDateRange.bind(this);
        this.onChangeFilterNumRange = this.onChangeFilterNumRange.bind(this);
    }

    onChangeOrder(columnName) {
        this.setState(prevState => {
            if(prevState.order.name !== columnName) {
                return {order: {name: columnName, direction: true}};
            } else {
                switch (prevState.order.direction) {
                    case undefined:
                        return {order: {name: columnName, direction: true}};
                    case true:
                        return {order: {name: columnName, direction: false}};
                    case false:
                        return {order: {name: columnName, direction: undefined}};
                    default:
                        return {order: prevState.order};
                }
            }
        });
    }

    onChangeFilter(columnName, filter) {
        this.setState(prevState => {
            return {
                filter: Object.assign({}, prevState.filter, {[columnName]: filter})
            }
        })
    }
    onChangeFilterDateRange(columnName, startDate, endDate) {
        this.setState(prevState => {
            return {
                filter: Object.assign({}, prevState.filter, {[columnName]: {startDate, endDate}})
            }
        });
    }
    onChangeFilterNumRange(columnName, start, end) {
        this.setState(prevState => {
            return {
                filter: Object.assign({}, prevState.filter, {[columnName]: {start, end}})
            }
        });
    }
    render() {
        let {list, isLogin, inProgress} = this.props;
        if(!isLogin) {
            return <Redirect to="/login" />
        }
        if(inProgress) {
            return <h4 className="text-center mt-5">Loading...</h4>
        }
        if(!list || !list.length) {
            return <h4 className="text-center mt-5">You have not transaction</h4>
        }

        function getSortFloatFunc(prop, direction){
            return (t1, t2) => {
                if (direction) {
                    return parseFloat(t2[prop]) - parseFloat(t1[prop]);
                } else {
                    return parseFloat(t1[prop]) - parseFloat(t2[prop]);
                }
            }
        }
        let filterByTextProps = (value, ...props) => {
            let result = true;
            for (let prop of props) {
                let filterValue = this.state.filter[prop];
                if(filterValue !== undefined && !(value[prop] + '').includes(filterValue)) {
                    result = false;
                }
            }
            return result;
        };
        let filterByDateRangeProp = (value, ...props) => {
            let result = true;
            for (let prop of props) {
                if(this.state.filter[prop]) {
                    let startDate = this.state.filter[prop].startDate;
                    let endDate = this.state.filter[prop].endDate;
                    if (startDate !== undefined && startDate > moment(value[prop])) {
                        result = false;
                    }
                    if (endDate !== undefined && moment(value[prop]) > endDate) {
                        result = false;
                    }
                }
            }
            return result;
        };
        let filterByNumRangeProp = (value, ...props) => {
            let result = true;
            for (let prop of props) {
                if(this.state.filter[prop]) {
                    let start = this.state.filter[prop].start;
                    let end = this.state.filter[prop].end;
                    let propVal = parseFloat(value[prop]);
                    if (!Number.isNaN(start) && !Number.isNaN(propVal) && start > propVal) {
                        result = false;
                    }
                    if (!Number.isNaN(end) && !Number.isNaN(propVal) && propVal > end) {
                        result = false;
                    }
                }
            }
            return result;
        };

        let sortFn = (t1, t2) => {
            let d1 = new Date(t1.date);
            let d2 = new Date(t2.date);
            return d2.getTime() - d1.getTime()
        };
        if(this.state.order.direction !== undefined) {
            switch (this.state.order.name) {
                case 'id':
                case 'amount':
                case 'balance':
                    sortFn = getSortFloatFunc(this.state.order.name, this.state.order.direction);
                    break;
                case 'date':
                    sortFn = (t1, t2) => {
                        let d1 = new Date(t1.date);
                        let d2 = new Date(t2.date);
                        if(this.state.order.direction) {
                            return d2.getTime() - d1.getTime()
                        } else {
                            return d1.getTime() - d2.getTime();
                        }
                    };
                    break;
                case 'username':
                    sortFn = (t1, t2) => {
                        let k = this.state.order.direction ? 1 : -1;
                        return t2.username.localeCompare(t1.username) * k;
                    };
                    break;
                default:
                    break;
            }
        }
        list = list.filter(t => {
            return filterByTextProps(t, 'id', 'username') &&
                    filterByDateRangeProp(t, 'date') &&
                    filterByNumRangeProp(t, 'amount', 'balance');
        });
        list = list.sort(sortFn);
        return (
            <div className="transactions">
                <Table bordered striped>
                    <thead className="thead-dark">
                        <tr>
                            <th><FilterHeader name="ID" type="text"
                                              order={this.state.order.name === 'id' ? this.state.order.direction : undefined}
                                              onChangeOrder={this.onChangeOrder} onChangeFilter={this.onChangeFilter}
                                              onClose={this.onChangeFilter}/></th>
                            <th><FilterHeader name="Date" type="date-range" onChangeOrder={this.onChangeOrder}
                                              order={this.state.order.name === 'date' ? this.state.order.direction : undefined}
                                              onChangeFilter={this.onChangeFilterDateRange}
                                              onClose={this.onChangeFilterDateRange}/></th>
                            <th><FilterHeader name="Username" type="text"
                                              order={this.state.order.name === 'username' ? this.state.order.direction : undefined}
                                              onChangeOrder={this.onChangeOrder} onChangeFilter={this.onChangeFilter}
                                              onClose={this.onChangeFilter}/></th>
                            <th><FilterHeader name="Amount" type="num-range"
                                              order={this.state.order.name === 'amount' ? this.state.order.direction : undefined}
                                              onChangeOrder={this.onChangeOrder} onChangeFilter={this.onChangeFilterNumRange}
                                              onClose={this.onChangeFilterNumRange}/></th>
                            <th><FilterHeader name="Balance" type="num-range"
                                              order={this.state.order.name === 'balance' ? this.state.order.direction : undefined}
                                              onChangeOrder={this.onChangeOrder} onChangeFilter={this.onChangeFilterNumRange}
                                              onClose={this.onChangeFilterNumRange}/></th>
                            <th>Repeat</th>
                        </tr>
                    </thead>
                    <tbody>
                        { list.length ?
                            list.map(t => {
                                return (
                                    <tr key={t.id}>
                                        <td><HighlightText text={t.id} value={this.state.filter.id} /></td>
                                        <td>{t.date}</td>
                                        <td><HighlightText text={t.username} value={this.state.filter.username} /> </td>
                                        <td>{t.amount}</td>
                                        <td>{t.balance}</td>
                                        <td><Link to={`/create-transaction/${t.username}/${Math.abs(t.amount)}`}>Repeat</Link></td>
                                    </tr>
                                );
                            }) :
                            <tr>
                                <td colSpan={6} className="text-center">You have not transaction with that filter</td>
                            </tr>
                        }
                    </tbody>
                </Table>
            </div>
        );
    }
}

export default Transactions;
