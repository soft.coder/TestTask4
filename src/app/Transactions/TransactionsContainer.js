import { connect } from 'react-redux';
import Transactions from './Transactions';
import {transactions} from './TransactionsActions';


const mapDispatchToProps = (dispatch) => {
    return {
        transactions() {
            dispatch(transactions());
        }
    }
};
const mapStateToProps = state => {
    return {
        list: state.transactions.list,
        isLogin: state.userState.isLogin,
        inProgress: state.transactions.inProgress
    }
};

const TransactionsContainer = connect(
    mapStateToProps,
    mapDispatchToProps
)(Transactions);

export default TransactionsContainer