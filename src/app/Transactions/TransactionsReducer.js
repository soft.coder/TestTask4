import {TRANSACTIONS_FAILURE, TRANSACTIONS_REQUEST, TRANSACTIONS_SUCCESS} from './TransactionsActionTypes';

function transactions(state = [], action) {
    switch(action.type) {
        case TRANSACTIONS_REQUEST:
            return Object.assign({}, state, {inProgress: true});
        case TRANSACTIONS_SUCCESS:
            return Object.assign({}, state, {inProgress: false, list: action.list});
        case TRANSACTIONS_FAILURE:
            return Object.assign({}, state, {inProgress: false});
        default:
            return state;
    }
}
export default transactions;